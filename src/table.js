var MAX_ROW_COUNT = 10;

function Table() {
    var tableBody = document.getElementById('table-body');

    this.addRow = function(number, color) {
        var row = document.createElement('tr');
        var numberCol = document.createElement('td');
        numberCol.innerText = number;
        var colorCol = document.createElement('td');
        colorCol.innerText = color;
        row.appendChild(numberCol);
        row.appendChild(colorCol);
        tableBody.insertBefore(row, tableBody.firstChild);
        removeExtraRow();
    };

    function removeExtraRow() {
        if (tableBody.childNodes.length > MAX_ROW_COUNT) {
            tableBody.removeChild(tableBody.lastElementChild);
        }
    }
}

module.exports = new Table();