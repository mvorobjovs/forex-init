(function() {
    window.onload = function() {
        //initialising element listeners
        require('./table');
        
        var setup = require('./setup');
        var ui = require('./ui');

        setup.init();
        ui.init();
    };
})();