var Blue = require('./classificators/BlueClassificator');
var White = require('./classificators/WhiteClassificator');
var Yellow = require('./classificators/YellowClassificator');
var YellowBlue = require('./classificators/YellowBlueClassificator');

function Classify() {
    var classificators = [
        new Blue(),
        new White(),
        new Yellow(),
        new YellowBlue()
    ];

    this.do = function(number) {
        var result = false;
        for (var i = 0; i < classificators.length; i++) {
            result = classificators[i].classify(number);
            if (result) break;
        }
        return result;
    };
}

module.exports = new Classify();