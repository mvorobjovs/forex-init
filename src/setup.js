var table = require('./table');
var classify = require('./classify');

function Setup() {
    this.init = function() {
        $.getJSON("/numbers", function(data) {
            addRowsOnSuccess(data);
        });
    };

    function addRowsOnSuccess(data) {
        for (var i = 0; i<data.data.length; i++) {
            table.addRow(
                data.data[i],
                classify.do(data.data[i])
            )
        }
    }
}

module.exports = new Setup();