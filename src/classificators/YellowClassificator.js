var Classificator = require('./Classificator');

function YellowClassificator() {
    var COLOR = 'yellow';
    Classificator.call(this, COLOR);

    this.classify = function(number) {
        this.number(number);
        return (this.isInteger() &&
            this.isDivisible(3) &&
            !this.isDivisible(5)) ? COLOR : false;
    }
}

module.exports = YellowClassificator;