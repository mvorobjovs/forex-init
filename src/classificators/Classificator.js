var helperFunctions = require('../bin/helperFunctions')

function Classificator(color) {
    var classifiedNumber;
    var classifiedColor = color;

    this.number = function(number) {
        if (!arguments.length) return classifiedNumber;
        classifiedNumber = number;
    };

    this.color = function(color) {
        return classifiedColor;
    };

    this.isInteger = function() {
        return helperFunctions.isInteger(classifiedNumber);
    };

    this.isDivisible = function(divider) {
        return helperFunctions.isInteger(classifiedNumber / divider);
    }
}

module.exports = Classificator;