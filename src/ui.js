var helperFunctions = require('./bin/helperFunctions');
var classify = require('./classify');
var table = require('./table');

function Ui() {
    var self = this;
    self.validInput = false;

    this.init = function() {
        self.form = document.forms['form-primary'];
        self.numberInput = document.getElementById('number');
        self.warningMessage = document.getElementById('warning');
        self.submitButton = document.getElementById('submit');
        listenForm();
        listenInput();
    };

    function listenForm() {
        self.form.addEventListener("submit", function(event) {
            sendNumber();
            event.preventDefault();
        });
    }

    function listenInput() {
        self.numberInput.addEventListener('keyup', function () {
            validateInput();
        });
    }
    
    function validateInput() {
        self.validInput = helperFunctions.isInteger(self.numberInput.value);
        if (self.validInput) {
            self.warningMessage.style.display = 'none';
            self.submitButton.disabled = false;
        } else {
            self.warningMessage.style.display = 'inline';
            self.submitButton.disabled = true;
        }
    }

    function sendNumber() {
        if (self.validInput) {
            $.post("/numbers", $(self.form).serialize(), function() {
                addRowOnSuccess();
            });
        }
    }
    
    function addRowOnSuccess() {
        table.addRow(
            self.numberInput.value,
            classify.do(self.numberInput.value)
        );
        self.numberInput.value = "";
    }
}

module.exports = new Ui;