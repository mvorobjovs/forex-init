module.exports.isInteger = function(number) {
    return /^\d+$/.test(number);
};