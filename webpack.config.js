//watch change and recompile command 'webpack --progress --colors --watch'
var path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: [
        './src/index'
    ],
    output: {
        path: './public',
        filename: 'bundle.js'
    }
};