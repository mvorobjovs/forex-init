var express = require('express');
var router = express.Router();
var numberStorage = require('../app/NumberStorage');

router.get('/', function(req, res, next) {
  res.render('index');
});

router.post('/numbers', function(req, res, next) {
    numberStorage.setNumber(req.body.number);
    res.end();
});

router.get('/numbers', function(req, res) {
    res.json({data: numberStorage.getNumbers()});
});

module.exports = router;
