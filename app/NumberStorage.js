var numberArray = [];
var MAX_COUNT = 10;

function setNumber(number) {
    numberArray.push(number);
    if (numberArray.length > MAX_COUNT) {
        numberArray = numberArray.slice(1,MAX_COUNT);
    }
}

function getNumbers() {
    return numberArray;
}

exports.setNumber = setNumber;
exports.getNumbers = getNumbers;