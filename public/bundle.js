/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	(function() {
	    window.onload = function() {
	        //initialising element listeners
	        __webpack_require__(13);
	        
	        var setup = __webpack_require__(14);
	        var ui = __webpack_require__(11);
	
	        setup.init();
	        ui.init();
	    };
	})();

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var Classificator = __webpack_require__(3);
	
	function BlueClassificator() {
	    var COLOR = 'blue';
	    Classificator.call(this, COLOR);
	
	    this.classify = function(number) {
	        this.number(number);
	        return (this.isInteger() &&
	            !this.isDivisible(3) &&
	            this.isDivisible(5)) ? COLOR : false;
	    }
	}
	
	module.exports = BlueClassificator;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var helperFunctions = __webpack_require__(4)
	
	function Classificator(color) {
	    var classifiedNumber;
	    var classifiedColor = color;
	
	    this.number = function(number) {
	        if (!arguments.length) return classifiedNumber;
	        classifiedNumber = number;
	    };
	
	    this.color = function(color) {
	        return classifiedColor;
	    };
	
	    this.isInteger = function() {
	        return helperFunctions.isInteger(classifiedNumber);
	    };
	
	    this.isDivisible = function(divider) {
	        return helperFunctions.isInteger(classifiedNumber / divider);
	    }
	}
	
	module.exports = Classificator;

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports.isInteger = function(number) {
	    return /^\d+$/.test(number);
	};

/***/ },
/* 5 */,
/* 6 */,
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var Classificator = __webpack_require__(3);
	
	function WhiteClassificator() {
	    var COLOR = 'white';
	    Classificator.call(this, COLOR);
	
	    this.classify = function(number) {
	        this.number(number);
	        return (this.isInteger() &&
	            !this.isDivisible(3) &&
	            !this.isDivisible(5)) ? COLOR : false;
	    }
	}
	
	module.exports = WhiteClassificator;

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var Classificator = __webpack_require__(3);
	
	function YellowClassificator() {
	    var COLOR = 'yellow';
	    Classificator.call(this, COLOR);
	
	    this.classify = function(number) {
	        this.number(number);
	        return (this.isInteger() &&
	            this.isDivisible(3) &&
	            !this.isDivisible(5)) ? COLOR : false;
	    }
	}
	
	module.exports = YellowClassificator;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var Classificator = __webpack_require__(3);
	
	function YellowBlueClassificator() {
	    var COLOR = 'yellowblue';
	    Classificator.call(this, COLOR);
	
	    this.classify = function(number) {
	        this.number(number);
	        return (this.isInteger() &&
	            this.isDivisible(3) &&
	            this.isDivisible(5)) ? COLOR : false;
	    }
	}
	
	module.exports = YellowBlueClassificator;

/***/ },
/* 10 */,
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var helperFunctions = __webpack_require__(4);
	var classify = __webpack_require__(12);
	var table = __webpack_require__(13);
	
	function Ui() {
	    var self = this;
	    self.validInput = false;
	
	    this.init = function() {
	        self.form = document.forms['form-primary'];
	        self.numberInput = document.getElementById('number');
	        self.warningMessage = document.getElementById('warning');
	        self.submitButton = document.getElementById('submit');
	        listenForm();
	        listenInput();
	    };
	
	    function listenForm() {
	        self.form.addEventListener("submit", function(event) {
	            sendNumber();
	            event.preventDefault();
	        });
	    }
	
	    function listenInput() {
	        self.numberInput.addEventListener('keyup', function () {
	            validateInput();
	        });
	    }
	    
	    function validateInput() {
	        self.validInput = helperFunctions.isInteger(self.numberInput.value);
	        if (self.validInput) {
	            self.warningMessage.style.display = 'none';
	            self.submitButton.disabled = false;
	        } else {
	            self.warningMessage.style.display = 'inline';
	            self.submitButton.disabled = true;
	        }
	    }
	
	    function sendNumber() {
	        if (self.validInput) {
	            $.post("/numbers", $(self.form).serialize(), function() {
	                addRowOnSuccess();
	            });
	        }
	    }
	    
	    function addRowOnSuccess() {
	        table.addRow(
	            self.numberInput.value,
	            classify.do(self.numberInput.value)
	        );
	        self.numberInput.value = "";
	    }
	}
	
	module.exports = new Ui;

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var Blue = __webpack_require__(2);
	var White = __webpack_require__(7);
	var Yellow = __webpack_require__(8);
	var YellowBlue = __webpack_require__(9);
	
	function Classify() {
	    var classificators = [
	        new Blue(),
	        new White(),
	        new Yellow(),
	        new YellowBlue()
	    ];
	
	    this.do = function(number) {
	        var result = false;
	        for (var i = 0; i < classificators.length; i++) {
	            result = classificators[i].classify(number);
	            if (result) break;
	        }
	        return result;
	    };
	}
	
	module.exports = new Classify();

/***/ },
/* 13 */
/***/ function(module, exports) {

	var MAX_ROW_COUNT = 10;
	
	function Table() {
	    var tableBody = document.getElementById('table-body');
	
	    this.addRow = function(number, color) {
	        var row = document.createElement('tr');
	        var numberCol = document.createElement('td');
	        numberCol.innerText = number;
	        var colorCol = document.createElement('td');
	        colorCol.innerText = color;
	        row.appendChild(numberCol);
	        row.appendChild(colorCol);
	        tableBody.insertBefore(row, tableBody.firstChild);
	        removeExtraRow();
	    };
	
	    function removeExtraRow() {
	        if (tableBody.childNodes.length > MAX_ROW_COUNT) {
	            tableBody.removeChild(tableBody.lastElementChild);
	        }
	    }
	}
	
	module.exports = new Table();

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var table = __webpack_require__(13);
	var classify = __webpack_require__(12);
	
	function Setup() {
	    this.init = function() {
	        $.getJSON("/numbers", function(data) {
	            addRowsOnSuccess(data);
	        });
	    };
	
	    function addRowsOnSuccess(data) {
	        for (var i = 0; i<data.data.length; i++) {
	            table.addRow(
	                data.data[i],
	                classify.do(data.data[i])
	            )
	        }
	    }
	}
	
	module.exports = new Setup();

/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map