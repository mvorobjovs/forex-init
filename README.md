# HOW TO INSTALL AND RUN #

0. Get current repo
1. Get Node.js v6.2.1 https://nodejs.org/en/download/
2. $ npm i
3. $ npm install -g bower
4. $ cd public/vendor
5. $ bower install 
6. $ cd ../../
7. $ node bin/www

# JS SOURCE CODE #
JavaScript source code is located in **./src
**

JavaScript is bundled using WebPack https://webpack.github.io/